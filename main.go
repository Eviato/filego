package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
)

func main() {
	recursivePtr := flag.Bool("r", false, "pass into subdirectory or not")
	folderPtr := flag.String("d", "", "where to launch the command")
	filePtr := flag.String("f", "", "specify one file")
	helpPtr := flag.Bool("h", false, "display help")

	flag.Parse()
	if *folderPtr == "" && *filePtr == "" {
		displayHelp()
	} else {
		handleFlags(*recursivePtr, *folderPtr, *filePtr, *helpPtr)
	}
}

func handleFlags(recursivePtr bool, folderPtr string, filePtr string, helpPtr bool) {
	if helpPtr == true {
		displayHelp()
	} else {
		if filePtr != "" && matchExtension(filePtr) == true {
			consoleLog("just a file")
		} else {
			files, err := ioutil.ReadDir(folderPtr)
			if err != nil {
				log.Fatal(err)
			}

			res := loopFiles(files, folderPtr, recursivePtr)
			fmt.Println(res)
		}
	}
}

func displayHelp() {
	consoleLog("Welcome To Filego \n")
	consoleLog("This tools is here to help you managing the names of you files. \n")
	consoleLog("USAGE: \n")
	consoleLog("filego [-d -f -r -h] \n")
	consoleLog("-d : specify a directory \n")
	consoleLog("-f : specify a file \n")
	consoleLog("-r : when specify a directory use recurisivity \n")
	consoleLog("-h : display this help \n")
}

func loopFiles(files []os.FileInfo, folderName string, recursive bool) []string {
	var videos []string

	for _, f := range files {
		if isDirectory(f) == true && recursive == true {
			fullPath := folderName + "/" + f.Name()
			subFiles, err := ioutil.ReadDir(fullPath)
			if err != nil {
				log.Fatal(err)
			}
			resSub := loopFiles(subFiles, fullPath, recursive)
			videos = append(videos, resSub...)
		} else {
			res := matchExtension(f.Name())
			if res == true {
				videos = append(videos, f.Name())
			}
		}
	}
	return videos
}

func isDirectory(test os.FileInfo) bool {
	return test.IsDir()
}

func matchExtension(filename string) bool {
	// Extensions file to map
	extension := "avi|mkv|mp4|txt"

	matched, err := regexp.MatchString("^.*.("+extension+")", filename)
	if err == nil && matched == true {
		return true
	}
	return false
}

func consoleLog(element string) {
	fmt.Println(element)
}
